import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from './base.service';
import { Configuration } from './configuration';
@Injectable()
export class CallService extends BaseService {

	constructor(http: Http) {
    	super(http);
	}
	getContact (){
		return this.getData(Configuration.BASE_URL + Configuration.PERSON);
	}

}
